# JSE TASK MANAGER

## About Application

Task Manager Application

## About Developer

* **Full Name**: Ruslan Abzalov
* **E-Mail**: ruslanonetwo@gmail.com
* **Company**: Technoserv Consulting

## Hardware

### Work Laptop

* **CPU**: Intel Core i5-7200U @ 2.50 GHz x 4
* **RAM**: 8 GB
* **Disk Capacity**: SSD 256 GB

### Home Laptop

* **CPU**: Intel Core i7-7500U @ 2.70 GHz x 4
* **RAM**: 8 GB
* **Disk Capacity**: SSD 256 GB

## Software

### Work Laptop

* **OS Name**: Windows 10 Enterprise LTSC 1809
* **OS Type**: 64-bit
* **JDK**: 1.8+

### Home Laptop

* **OS Name**: Windows 10 Home Single Language 20H2
* **OS Type**: 64-bit
* **JDK**: 1.8+

## How To Build

```
mvn clean install
```

## How To Run

```
java -jar target/task-manager.jar
```

## Tasks

[TSC JSE](https://1drv.ms/u/s!ApZHq0-L1WVQgYon3yurFX5v-E0d4A?e=0k2k1d)
