package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public class SystemUtil {

    public static long getApplicationPid() {
        @Nullable val processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            }
            catch (@NotNull final Exception exception) {
                return 0;
            }
        }
        return 0;
    }

}
