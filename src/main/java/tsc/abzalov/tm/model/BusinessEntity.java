package tsc.abzalov.tm.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Status;

import java.time.LocalDateTime;
import java.util.Optional;

import static tsc.abzalov.tm.util.LiteralConst.*;
import static tsc.abzalov.tm.enumeration.Status.TODO;
import static tsc.abzalov.tm.util.Formatter.DATE_TIME_FORMATTER;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BusinessEntity extends AbstractEntity {
    
    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = TODO;

    @Nullable
    private LocalDateTime startDate;

    @Nullable
    private LocalDateTime endDate;

    @Nullable
    private String userId;

    @Override
    @NotNull
    public String toString() {
        @NotNull val correctName = Optional.ofNullable(name).orElse(DEFAULT_NAME);
        @NotNull val correctDescription = Optional.ofNullable(description).orElse(DEFAULT_DESCRIPTION);
        @NotNull val correctStartDate = (startDate == null)
                ? IS_NOT_STARTED
                : startDate.format(DATE_TIME_FORMATTER);
        @NotNull val correctEndDate = (endDate == null)
                ? IS_NOT_ENDED
                : endDate.format(DATE_TIME_FORMATTER);
        @NotNull val correctUserId = Optional.ofNullable(userId).orElse(DEFAULT_REFERENCE);

        return correctName +
                ": [ID: " + getId() +
                "; Description: " + correctDescription +
                "; Status: " + status.getStatusName() +
                "; Start Date: " + correctStartDate +
                "; End Date: " + correctEndDate +
                "; User ID: " + correctUserId + "]";
    }

}

