package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

@SuppressWarnings("unused")
public final class ProjectStartByIdCommand extends AbstractCommand {

    public ProjectStartByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "start-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("START PROJECT BY ID");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @Nullable val project = projectService.startById(currentUserId, inputId());
            val wasProjectStarted = Optional.ofNullable(project).isPresent();
            if (wasProjectStarted) {
                System.out.println("Project was successfully started.\n");
                return;
            }

            System.out.println("Project was not started! Please, check that project exists or it has correct status.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
