package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IApplicationPropertyService extends IDeveloperInfoPropertyService, IHashingPropertyService {

    void initLocalProperties();

    @NotNull
    String getAppVersion();

}
