package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void initCommands(@Nullable IServiceLocator serviceLocator);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getStartupCommands();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArguments();

    @NotNull
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    AbstractCommand getArgumentByName(@Nullable String name);

}
